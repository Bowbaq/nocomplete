var bkg = chrome.extension.getBackgroundPage();

// Populate storage on install
chrome.runtime.onInstalled.addListener(function(details){
  if (details.reason == "install"){
    chrome.storage.sync.clear(function() {
      if (chrome.runtime.lastError) {
        bkg.console.log("Runtime error:", chrome.runtime.lastError);
      }
    });
    chrome.storage.sync.set({ "domainMatches": {}, "urlMatches": {} }, function() {
      if (chrome.runtime.lastError) {
        bkg.console.log("Runtime error:", chrome.runtime.lastError);
      }
    });
  }
});

var domainMatches = {};
var urlMatches = {};

chrome.storage.sync.get(null, function(data) {
  if (chrome.runtime.lastError) {
    bkg.console.log("Runtime error:", chrome.runtime.lastError);
    return
  }

  domainMatches = data.domainMatches;
  urlMatches = data.urlMatches;
});

chrome.storage.sync.onChanged.addListener(function(changes, namespace) {
  for (var key in changes) {
    if (key === "domainMatches") {
      domainMatches = changes[key].newValue;
    }
    if (key === "urlMatches") {
      urlMatches = changes[key].newValue;
    }
  }
});

// Injects code when URL matches
chrome.tabs.onUpdated.addListener(function(tabID, changeInfo, tab){
  var url = new URL(tab.url)
  if (! url.protocol.startsWith("http")) {
    bkg.console.log("not https(s)", tabID)
    return
  }

  var sanitizedURL = (url.origin + url.pathname).replace(/\/+$/, "");
  bkg.console.log(domainMatches, urlMatches);
  if (!domainMatches[url.hostname] && !urlMatches[sanitizedURL]) {
    bkg.console.log("doesn't match", tabID, domainMatches[url.hostname], urlMatches[sanitizedURL], sanitizedURL)
    return
  }

  bkg.console.log("matches", tabID, domainMatches[url.hostname], urlMatches[sanitizedURL], sanitizedURL)
  // injected[tabID] = true
  chrome.tabs.executeScript(tabID, {file: "nocomplete.js"})
});
