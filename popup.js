function withURL(callback) {
  chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
    var tab = tabs[0];
    var url = new URL(tab.url)
    var sanitizedURL = (url.origin + url.pathname).replace(/\/+$/, "");

    callback(url, sanitizedURL)
  });
}

function updateButtons(url, sanitizedURL, data) {
  var domainButton = document.getElementById("domain");
  var pageButton = document.getElementById("page");

  if (data.domainMatches[url.hostname]) {
    domainButton.innerText = "Disable for this domain";
    domainButton.setAttribute("data-disable", "true");
  } else {
    domainButton.innerText = "Enable for this domain";
    domainButton.removeAttribute("data-disable");
  }

  if (data.urlMatches[sanitizedURL]) {
    pageButton.innerText = "Disable for this page";
    pageButton.setAttribute("data-disable", "true");
  } else {
    pageButton.innerText = "Enable for this page";
    pageButton.removeAttribute("data-disable");
  }
}

document.body.onload = function() {
  withURL(function(url, sanitizedURL){
    if (! url.protocol.startsWith("http")) {
      console.log("not https(s)")
      return
    }

    chrome.storage.sync.get(null, function(data) {
      if (chrome.runtime.lastError) {
        console.log("Runtime error:", chrome.runtime.lastError);
        return
      }

      updateButtons(url, sanitizedURL, data);
    });
  });
}

document.getElementById("domain").addEventListener("click", function(e) {
  withURL(function(url, sanitizedURL){
    if (! url.protocol.startsWith("http")) {
      console.log("not https(s)")
      return
    }

    chrome.storage.sync.get(null, function(data) {
      console.log("domainMatches", data.domainMatches, url, sanitizedURL)

      if (chrome.runtime.lastError) {
        console.log("Runtime error:", chrome.runtime.lastError);
        return
      }

      console.log(e.target.attributes)
      if (e.target.getAttribute("data-disable") === "true") {
        console.log("deleting", url.hostname);
        delete data.domainMatches[url.hostname];
      } else {
        console.log("adding", url.hostname);
        data.domainMatches[url.hostname] = true;
      }

      chrome.storage.sync.set(data, function() {
        if (chrome.runtime.lastError) {
          console.log("Runtime error:", chrome.runtime.lastError);
        }

        updateButtons(url, sanitizedURL, data);
      });
    });
  });
});

document.getElementById("page").addEventListener("click", function(e) {
  withURL(function(url, sanitizedURL){
    if (! url.protocol.startsWith("http")) {
      console.log("not https(s)")
      return
    }

    chrome.storage.sync.get(null, function(data) {
      console.log("urlMatches", data.urlMatches, url, sanitizedURL)

      if (chrome.runtime.lastError) {
        console.log("Runtime error:", chrome.runtime.lastError);
        return
      }

      console.log(e.target.attributes)
      if (e.target.getAttribute("data-disable") === "true") {
        console.log("deleting", sanitizedURL);
        delete data.urlMatches[sanitizedURL];
      } else {
        console.log("adding", sanitizedURL);
        data.urlMatches[sanitizedURL] = true;
      }

      chrome.storage.sync.set(data, function() {
        if (chrome.runtime.lastError) {
          console.log("Runtime error:", chrome.runtime.lastError);
        }

        updateButtons(url, sanitizedURL, data);
      });
    });
  });
});
